`warcdedupe`: a tool and libraries for manipulating and deduplicating
[WARC files](https://iipc.github.io/warc-specifications/specifications/warc-format/warc-1.1/).

## `warcdedupe`

Given an input WARC file, output a copy with duplicate response records
rewritten to be revisit records pointing to the first instance of that response
in the same file.

```
A library and tool for deduplicating WARC records

USAGE:
    warcdedupe [OPTIONS] <infile> [outfile]

ARGS:
    <infile>     Name of file to read
    <outfile>    Name of file to write, stdout if omitted or '-'

OPTIONS:
        --compress-output     Write compressed records to non-file output
        --disable-mmap        Never memory-map the input, even if possible
        --disable-progress    Do not display progress, even to a terminal
    -h, --help                Print help information
    -V, --version             Print version information

When output is a file, compression options are ignored. Input and output
files are assumed to be compressed if the file name ends in '.gz'.
```

Disclaimer: **this tool's behavior is not yet well-tested**: although it
appears to correctly deduplicate records and write valid WARC files, its
correctness and validity of output files may not be standards-compliant.
Users are strongly advised to test its correctness if the results will be
used to replace the original inputs for long-term storage.

## `warcio`

The `warcio` crate is intended to be suitable for general use in programs that
need to read or write WARC files, similar to the [Python library of the same
name](https://pypi.org/project/warcio/).

At present its API is designed according to the needs of `warcdedupe`, but
suggestions or commentary on ways it may be improved for general use are
welcome.

## Performance

Good performance is a goal of this tool. Within reason, additional
complexity is acceptable where it results in improved runtime performance.

In `warcdedupe`, command line options are offered to disable some ergonomic
features that have runtime cost. In particular, enabling progress reporting
(the default unless a terminal is not available, turned off with
`--disable-progress`) incurs a performance hit of around 5% in tests. In some
situations it may also be useful to avoid memory-mapping an input file
(`--disable-mmap`) and instead use regular `read` operations copying data into
memory, though the performance effects of doing so are strongly dependent
on the underlying storage (however if operating on files that may be
concurrently modified, avoiding `mmap` also ensures concurrent modifications
cannot cause `warcdedupe` to crash as a result).

When possible, `warcdedupe` avoids unnecessary compression or decompression of
records when the input and output use the same compression (if both are
uncompressed or both are compressed with gzip, for instance). This requires
support for random access to inputs (usually implying the input must be a
regular file).

### Optimization

As is typical for Rust programs, performance is not very good in debug builds,
which are the default for Cargo. In my testing, debug builds are around 50
times slower: **remember to build in release mode** for actual use!

When building `warcdedupe` you may select an alternate compression backend for
handling gzipped records via feature flags. Of particular interest, `zlib-ng`
is around 10% faster than the default choice for decompression, although it
does require a C compiler at build-time. You can use `zlib-ng` by turning on
the `flate2/zlib-ng-compat` feature:

    cargo build --release --features flate2/zlib-ng-compat

## Comparison with other tools

* [warc-dedup](https://github.com/ArchiveTeam/warc-dedup) is implemented in Python
  using the `warcio` library, and deduplicates against public archives in the Internet
  Archive's Wayback Machine. A network round-trip to IA for every record makes it quite
  slow, but deduplicating against those public archives may be desirable for some datasets.
  Measured performance of `warcdedupe` is about 5x faster than `warc-dedup` (12 vs 66
  seconds on a test WARC) and using about 1/3 as much memory, even after `warc-dedup`
  is patched to avoid consulting the public indexes.

* The ["optiwarc" toolset](https://gitlab.com/taricorp/optiwarc) (warcsum, warccollres,
  and warcrefs), described in [my 2016 blog post](https://www.taricorp.net/2016/web-history-warc/#deduplication),
  requires a number of manual steps to use but might work well for very large archives.

## Future improvements

 * Support for [CDXJ indexes](https://specs.webrecorder.net/cdxj/0.1.0/) (refer to
   [cdxj-indexer](https://pypi.org/project/cdxj-indexer/)) and
   [WACZ archives](https://specs.webrecorder.net/wacz/1.1.1/).
 * Parallel processing of records if an index is available.
 * Check behavior around <> surrounding some URIs:
   github.com/iipc/warc-specifications/pull/24
   Handle all of:
    * Correctly formed WARC 1.0 (with <>)
    * Malformed WARC 1.1 (with <>)
    * Correctly formed WARC 1.1 (without <>)
 * Attempt to provide an API compatible with or similar to the `warcio` Python module.
